module KWin.Workspace where

import Prelude

import Data.Maybe (Maybe(..))
import Effect (Effect)
import Foreign.Object (Object)
import KWin.KWin (ClientAreaOption, fromClientAreaOption)
import KWin.Slot (Signal1, Signal2, Signal3, Slot, Signal0)

type QSize = {w :: Int, h :: Int}

type QRect = {x :: Int, y :: Int, width :: Int, height :: Int}

type IntMap = Object

foreign import data Client :: Type

--------------
--PROPERTIES--
--------------

foreign import desktopGridSize :: Effect QSize
foreign import desktopGridWidth :: Effect Int
foreign import desktopGridHeight :: Effect Int
foreign import workspaceWidth :: Effect Int
foreign import workspaceHeight :: Effect Int
foreign import workspaceSize :: Effect QSize
foreign import displaySize :: Effect QSize
foreign import displayWidth :: Effect Int
foreign import displayHeight :: Effect Int
foreign import activeScreen :: Effect Int
foreign import numScreens :: Effect Int
foreign import currentActivity :: Effect String
foreign import activities :: Effect (Array String)
foreign import getCurrentDesktop :: Effect Int
foreign import setCurrentDesktop :: Int -> Effect Unit
foreign import getActiveClient :: Effect Client
foreign import setActiveClient :: Client -> Effect Unit
foreign import getDesktops :: Effect Int
foreign import setDesktops :: Int -> Effect Unit

---------
--SLOTS--
---------

foreign import desktopPresenceChanged :: Effect (Slot (Signal2 Client Int))
foreign import currentDesktopChanged :: Effect (Slot (Signal2 Int Client))
foreign import clientAdded :: Effect (Slot (Signal1 Client))
foreign import clientRemoved :: Effect (Slot (Signal1 Client))
foreign import clientManaging :: Effect (Slot (Signal1 Client))
foreign import clientMinimized :: Effect (Slot (Signal1 Client))
foreign import clientUnminimized :: Effect (Slot (Signal1 Client))
foreign import clientRestored :: Effect (Slot (Signal1 Client))
foreign import clientMaximizeSet :: Effect (Slot (Signal3 Client Boolean Boolean))
foreign import killWindowCalled :: Effect (Slot (Signal1 Client))
foreign import clientActivated :: Effect (Slot (Signal1 Client))
foreign import clientFullScreenSet :: Effect (Slot (Signal3 Client Boolean Boolean))
foreign import clientSetKeepAbove :: Effect (Slot (Signal2 Client Boolean))
foreign import numberDesktopsChanged :: Effect (Slot (Signal1 Int))
foreign import desktopLayoutChanged :: Effect (Slot Signal0)
foreign import clientDemandsAttentionChanged :: Effect (Slot (Signal2 Client Boolean))
foreign import numberScreensChanged :: Effect (Slot (Signal1 Int))
foreign import screenResized :: Effect (Slot (Signal1 Int))
foreign import currentActivityChanged :: Effect (Slot (Signal1 String))
foreign import activitiesChanged :: Effect (Slot (Signal1 String))
foreign import activityAdded :: Effect (Slot (Signal1 String))
foreign import activityRemoved :: Effect (Slot (Signal1 String))

-------------
--FUNCTIONS--
-------------

foreign import slotSwitchDesktopNext :: Effect Unit
foreign import slotSwitchDesktopPrevious :: Effect Unit
foreign import slotSwitchDesktopLeft :: Effect Unit
foreign import slotSwitchDesktopRight :: Effect Unit
foreign import slotSwitchDesktopUp :: Effect Unit
foreign import slotSwitchDesktopDown :: Effect Unit
foreign import slotSwitchToNextScreen :: Effect Unit
foreign import slotWindowToNextScreen :: Effect Unit
foreign import slotToggleShowDesktop :: Effect Unit
foreign import slotWindowMaximize :: Effect Unit
foreign import slotWindowMaximizeVertical :: Effect Unit
foreign import slotWindowMaximizeHorizontal :: Effect Unit
foreign import slotWindowMinimize :: Effect Unit
foreign import slotWindowShade :: Effect Unit
foreign import slotWindowRaise :: Effect Unit
foreign import slotWindowLower :: Effect Unit
foreign import slotWindowRaiseOrLower :: Effect Unit
foreign import slotActivateAttentionWindow :: Effect Unit
foreign import slotWindowPackLeft :: Effect Unit
foreign import slotWindowPackRight :: Effect Unit
foreign import slotWindowPackUp :: Effect Unit
foreign import slotWindowPackDown :: Effect Unit
foreign import slotWindowGrowHorizontal :: Effect Unit
foreign import slotWindowGrowVertical :: Effect Unit
foreign import slotWindowShrinkHorizontal :: Effect Unit
foreign import slotWindowShrinkVertical :: Effect Unit
foreign import slotWindowQuickTileLeft :: Effect Unit
foreign import slotWindowQuickTileRight :: Effect Unit
foreign import slotWindowQuickTileTopLeft :: Effect Unit
foreign import slotWindowQuickTileTopRight :: Effect Unit
foreign import slotWindowQuickTileBottomLeft :: Effect Unit
foreign import slotWindowQuickTileBottomRight :: Effect Unit
foreign import slotSwitchWindowUp :: Effect Unit
foreign import slotSwitchWindowDown :: Effect Unit
foreign import slotSwitchWindowRight :: Effect Unit
foreign import slotSwitchWindowLeft :: Effect Unit
foreign import slotIncreaseWindowOpacity :: Effect Unit
foreign import slotLowerWindowOpacity :: Effect Unit
foreign import slotWindowOperations :: Effect Unit
foreign import slotWindowClose :: Effect Unit
foreign import slotWindowMove :: Effect Unit
foreign import slotWindowResize :: Effect Unit
foreign import slotWindowAbove :: Effect Unit
foreign import slotWindowBelow :: Effect Unit
foreign import slotWindowOnAllDesktops :: Effect Unit
foreign import slotWindowFullScreen :: Effect Unit
foreign import slotWindowNoBorder :: Effect Unit
foreign import slotWindowToNextDesktop :: Effect Unit
foreign import slotWindowToPreviousDesktop :: Effect Unit
foreign import slotWindowToDesktopRight :: Effect Unit
foreign import slotWindowToDesktopLeft :: Effect Unit
foreign import slotWindowToDesktopUp :: Effect Unit
foreign import slotWindowToDesktopDown :: Effect Unit
foreign import showOutline :: QRect -> Effect Unit
foreign import hideOutline :: Effect Unit
foreign import clientList :: Effect (IntMap Client)
foreign import desktopName :: Int -> Effect String
foreign import supportInformation :: Effect String

getClient :: Int -> Effect (Maybe Client)
getClient i = getClientImpl i Just Nothing

clientArea :: ClientAreaOption -> Client -> Effect QRect
clientArea option = clientAreaImpl (fromClientAreaOption option)

-----------------------
--FFI IMPLEMENTATIONS--
-----------------------

foreign import getClientImpl :: Int -> (∀ a. a -> Maybe a) -> (∀ a. Maybe a) -> Effect (Maybe Client)
foreign import clientAreaImpl :: Int -> Client -> Effect QRect