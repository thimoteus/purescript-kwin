exports.unsafeGetProperty = function (prop) {
  return function () {
    return options[prop];
  }
}

exports.unsafeSetProperty = function (prop) {
  return function (val) {
    return function () {
      options[prop] = val;
    }
  }
}

exports.unsafeGetSlot = function (slotName) {
  return function () {
    return options[slotName];
  }
}