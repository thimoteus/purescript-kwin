module KWin.KWin where

import Prelude

import Data.Enum (class BoundedEnum, class Enum, defaultCardinality, defaultPred, defaultSucc)
import Data.Maybe (Maybe(..), fromJust)
import Effect (Effect)
import Partial.Unsafe (unsafePartial)

data ClientAreaOption
  = PlacementArea
  | MovementArea
  | MaximizeArea
  | MaximizeFullArea
  | FullScreenArea
  | WorkArea
  | FullArea
  | ScreenArea

derive instance eqClientAreaOption :: Eq ClientAreaOption
derive instance ordClientAreaOption :: Ord ClientAreaOption

instance enumClientAreaOption :: Enum ClientAreaOption where
  succ = defaultSucc toClientAreaOption fromClientAreaOption
  pred = defaultPred toClientAreaOption fromClientAreaOption

instance boundedClientAreaOption :: Bounded ClientAreaOption where
  bottom = PlacementArea
  top = ScreenArea

instance boundedEnumClientAreaOption :: BoundedEnum ClientAreaOption where
  cardinality = defaultCardinality
  toEnum = toClientAreaOption
  fromEnum = fromClientAreaOption

toClientAreaOption :: Int -> Maybe ClientAreaOption
toClientAreaOption = go
  where
  go 0 = Just PlacementArea
  go 1 = Just MovementArea
  go 2 = Just MaximizeArea
  go 3 = Just MaximizeFullArea
  go 4 = Just FullScreenArea
  go 5 = Just WorkArea
  go 6 = Just FullArea
  go 7 = Just ScreenArea
  go _ = Nothing

fromClientAreaOption :: ClientAreaOption -> Int
fromClientAreaOption = case _ of
  PlacementArea -> 0
  MovementArea -> 1 
  MaximizeArea -> 2 
  MaximizeFullArea -> 3 
  FullScreenArea -> 4 
  WorkArea -> 5 
  FullArea -> 6 
  ScreenArea -> 7 

foreign import placementAreaImpl :: Effect Int
foreign import movementAreaImpl :: Effect Int
foreign import maximizeAreaImpl :: Effect Int
foreign import maximizeFullAreaImpl :: Effect Int
foreign import fullScreenAreaImpl :: Effect Int
foreign import workAreaImpl :: Effect Int
foreign import fullAreaImpl :: Effect Int
foreign import screenAreaImpl :: Effect Int

placementArea :: Effect ClientAreaOption
placementArea = unsafePartial (fromJust <<< toClientAreaOption) <$> placementAreaImpl

movementArea :: Effect ClientAreaOption
movementArea = unsafePartial (fromJust <<< toClientAreaOption) <$> movementAreaImpl

maximizeArea :: Effect ClientAreaOption
maximizeArea = unsafePartial (fromJust <<< toClientAreaOption) <$> maximizeAreaImpl

maximizeFullArea :: Effect ClientAreaOption
maximizeFullArea = unsafePartial (fromJust <<< toClientAreaOption) <$> maximizeFullAreaImpl

fullScreenArea :: Effect ClientAreaOption
fullScreenArea = unsafePartial (fromJust <<< toClientAreaOption) <$> fullScreenAreaImpl

workArea :: Effect ClientAreaOption
workArea = unsafePartial (fromJust <<< toClientAreaOption) <$> workAreaImpl

fullArea :: Effect ClientAreaOption
fullArea = unsafePartial (fromJust <<< toClientAreaOption) <$> fullAreaImpl

screenArea :: Effect ClientAreaOption
screenArea = unsafePartial (fromJust <<< toClientAreaOption) <$> screenAreaImpl

data ElectricBorder
  = ElectricTop
  | ElectricTopRight
  | ElectricRight
  | ElectricBottomRight
  | ElectricBottom
  | ElectricBottomLeft
  | ElectricLeft
  | ElectricTopLeft
  | ELECTRIC_COUNT
  | ElectricNone

derive instance eqElectricBorder :: Eq ElectricBorder
derive instance ordElectricBorder :: Ord ElectricBorder

instance enumElectricBorder :: Enum ElectricBorder where
  succ = defaultSucc toElectricBorder fromElectricBorder
  pred = defaultPred toElectricBorder fromElectricBorder

instance boundedElectricBorder :: Bounded ElectricBorder where
  top = ElectricTop
  bottom = ElectricNone

instance boundedEnumElectricBorder :: BoundedEnum ElectricBorder where
  cardinality = defaultCardinality
  toEnum = toElectricBorder
  fromEnum = fromElectricBorder

toElectricBorder :: Int -> Maybe ElectricBorder
toElectricBorder = go
  where
  go 0 = Just ElectricTop
  go 1 = Just ElectricTopRight
  go 2 = Just ElectricRight
  go 3 = Just ElectricBottomRight
  go 4 = Just ElectricBottom
  go 5 = Just ElectricBottomLeft
  go 6 = Just ElectricLeft
  go 7 = Just ElectricTopLeft
  go 8 = Just ELECTRIC_COUNT
  go 9 = Just ElectricNone
  go _ = Nothing

fromElectricBorder :: ElectricBorder -> Int
fromElectricBorder = case _ of
  ElectricTop -> 0
  ElectricTopRight -> 1
  ElectricRight -> 2
  ElectricBottomRight -> 3
  ElectricBottom -> 4
  ElectricBottomLeft -> 5
  ElectricLeft -> 6
  ElectricTopLeft -> 7
  ELECTRIC_COUNT -> 8
  ElectricNone -> 9

foreign import electricTopImpl :: Effect Int
foreign import electricTopRightImpl :: Effect Int
foreign import electricRightImpl :: Effect Int
foreign import electricBottomRightImpl :: Effect Int
foreign import electricBottomImpl :: Effect Int
foreign import electricBottomLeftImpl :: Effect Int
foreign import electricLeftImpl :: Effect Int
foreign import electricTopLeftImpl :: Effect Int
foreign import electricCountImpl :: Effect Int
foreign import electricNoneImpl :: Effect Int

electricTop :: Effect ElectricBorder
electricTop = unsafePartial (fromJust <<< toElectricBorder) <$> electricTopImpl

electricTopRight :: Effect ElectricBorder
electricTopRight = unsafePartial (fromJust <<< toElectricBorder) <$> electricTopRightImpl

electricRight :: Effect ElectricBorder
electricRight = unsafePartial (fromJust <<< toElectricBorder) <$> electricRightImpl

electricBottomRight :: Effect ElectricBorder
electricBottomRight = unsafePartial (fromJust <<< toElectricBorder) <$> electricBottomRightImpl

electricBottom :: Effect ElectricBorder
electricBottom = unsafePartial (fromJust <<< toElectricBorder) <$> electricBottomImpl

electricBottomLeft :: Effect ElectricBorder
electricBottomLeft = unsafePartial (fromJust <<< toElectricBorder) <$> electricBottomLeftImpl

electricLeft :: Effect ElectricBorder
electricLeft = unsafePartial (fromJust <<< toElectricBorder) <$> electricLeftImpl

electricTopLeft :: Effect ElectricBorder
electricTopLeft = unsafePartial (fromJust <<< toElectricBorder) <$> electricTopLeftImpl

electricCount :: Effect ElectricBorder
electricCount = unsafePartial (fromJust <<< toElectricBorder) <$> electricCountImpl

electricNone :: Effect ElectricBorder
electricNone = unsafePartial (fromJust <<< toElectricBorder) <$> electricNoneImpl