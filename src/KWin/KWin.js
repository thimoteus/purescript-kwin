exports.placementAreaImpl = function () {
  return KWin.PlacementArea;
}

exports.movementAreaImpl = function () {
  return KWin.MovementArea;
}

exports.maximizeAreaImpl = function () {
  return KWin.MaximizeArea;
}

exports.maximizeFullAreaImpl = function () {
  return KWin.MaximizeFullArea;
}

exports.fullScreenAreaImpl = function () {
  return KWin.FullScreenArea;
}

exports.workAreaImpl = function () {
  return KWin.WorkArea;
}

exports.fullAreaImpl = function () {
  return KWin.FullArea;
}

exports.screenAreaImpl = function () {
  return KWin.ScreenArea;
}

exports.electricTopImpl = function () {
  return KWin.ElectricTop;
}

exports.electricTopRightImpl = function () {
  return KWin.ElectricTopRight;
}

exports.electricRightImpl = function () {
  return KWin.ElectricRight;
}

exports.electricBottomRightImpl = function () {
  return KWin.ElectricBottomRight;
}

exports.electricBottomImpl = function () {
  return KWin.ElectricBottom;
}

exports.electricBottomLeftImpl = function () {
  return KWin.ElectricBottomLeft;
}

exports.electricLeftImpl = function () {
  return KWin.ElectricLeft;
}

exports.electricTopLeftImpl = function () {
  return KWin.ElectricTopLeft;
}

exports.electricCountImpl = function () {
  return KWin.ELECTRIC_COUNT;
}

exports.electricNoneImpl = function () {
  return KWin.ElectricNone;
}
