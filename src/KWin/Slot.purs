module KWin.Slot where

import Prelude

import Effect (Effect)

foreign import data Slot :: Type -> Type
foreign import data SlotCloser :: Type

type Signal0 = Effect Unit
type Signal1 a = a -> Effect Unit
type Signal2 a b = a -> b -> Effect Unit
type Signal3 a b c = a -> b -> c -> Effect Unit
type Signal4 a b c d = a -> b -> c -> d -> Effect Unit
type Signal5 a b c d e = a -> b -> c -> d -> e -> Effect Unit

foreign import connect0 :: Slot Signal0 -> Signal0 -> Effect SlotCloser
foreign import connect1 :: ∀ a. Slot (Signal1 a) -> Signal1 a -> Effect SlotCloser
foreign import connect2 :: ∀ a b. Slot (Signal2 a b) -> Signal2 a b -> Effect SlotCloser
foreign import connect3 :: ∀ a b c. Slot (Signal3 a b c) -> Signal3 a b c -> Effect SlotCloser
foreign import connect4 :: ∀ a b c d. Slot (Signal4 a b c d) -> Signal4 a b c d -> Effect SlotCloser
foreign import connect5 :: ∀ a b c d e. Slot (Signal5 a b c d e) -> Signal5 a b c d e -> Effect SlotCloser
foreign import disconnect :: SlotCloser -> Effect Unit
