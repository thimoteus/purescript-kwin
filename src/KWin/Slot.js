exports.connect0 = function (slot) {
  return function (signal) {
    return function () {
      slot.connect(signal);
      return function () {
        slot.disconnect(signal);
      }
    }
  }
}

exports.connect1 = function (slot) {
  return function (signal) {
    return function () {
      var callback = function (a) {
        return signal(a)();
      }
      slot.connect(callback);
      return function () {
        slot.disconnect(callback);
      }
    }
  }
}

exports.connect2 = function (slot) {
  return function (signal) {
    return function () {
      var callback = function (a, b) {
        return signal(a)(b)();
      }
      slot.connect(callback);
      return function () {
        slot.disconnect(callback);
      }
    }
  }
}

exports.connect3 = function (slot) {
  return function (signal) {
    return function () {
      var callback = function (a, b, c) {
        return signal(a)(b)(c)();
      }
      slot.connect(callback);
      return function () {
        slot.disconnect(callback);
      }
    }
  }
}

exports.connect4 = function (slot) {
  return function (signal) {
    return function () {
      var callback = function (a, b, c, d) {
        return signal(a)(b)(c)(d)();
      }
      slot.connect(callback);
      return function () {
        slot.disconnect(callback);
      }
    }
  }
}

exports.connect5 = function (slot) {
  return function (signal) {
    return function () {
      var callback = function (a, b, c, d, e) {
        return signal(a)(b)(c)(d)(e)();
      }
      slot.connect(callback);
      return function () {
        slot.disconnect(callback);
      }
    }
  }
}

exports.disconnect = function (closer) {
  return function () {
    return closer();
  }
}