exports.desktopGridSize = function () {
  return workspace.desktopGridSize;
}

exports.desktopGridWidth = function () {
  return workspace.desktopGridWidth;
}

exports.desktopGridHeight = function () {
  return workspace.desktopGridHeight;
}

exports.workspaceWidth = function () {
  return workspace.workspaceWidth;
}

exports.workspaceHeight = function () {
  return workspace.workspaceHeight;
}

exports.workspaceSize = function () {
  return workspace.workspaceSize;
}

exports.displaySize = function () {
  return workspace.displaySize;
}

exports.displayWidth = function () {
  return workspace.displayWidth;
}

exports.displayHeight = function () {
  return workspace.displayHeight;
}

exports.activeScreen = function () {
  return workspace.activeScreen;
}

exports.numScreens = function () {
  return workspace.numScreens;
}

exports.currentActivity = function () {
  return workspace.currentActivity;
}

exports.activities = function () {
  return workspace.activities;
}

exports.getCurrentDesktop = function () {
  return workspace.currentDesktop;
} 

exports.setCurrentDesktop = function (desktop) {
  return function () {
    workspace.currentDesktop = desktop;
  }
}

exports.getActiveClient = function () {
  return workspace.activeClient;
}

exports.setActiveClient = function (client) {
  return function () {
    workspace.activeClient = client;
  }
}

exports.getDesktops = function () {
  return workspace.desktops;
}

exports.setDesktops = function (number) {
  return function () {
    workspace.desktops = number;
  }
}

exports.desktopPresenceChanged = function () {
  return workspace.desktopPresenceChanged;
}

exports.currentDesktopChanged = function () {
  return workspace.currentDesktopChanged;
}

exports.clientAdded = function () {
  return workspace.clientAdded;
}

exports.clientRemoved = function () {
  return workspace.clientRemoved;
}

exports.clientManaging = function () {
  return workspace.clientManaging;
}

exports.clientMinimized = function () {
  return workspace.clientMinimized;
}

exports.clientUnminimized = function () {
  return workspace.clientUnminimized;
}

exports.clientRestored = function () {
  return workspace.clientRestored;
}

exports.clientMaximizeSet = function () {
  return workspace.clientMaximizeSet;
}

exports.killWindowCalled = function () {
  return workspace.killWindowCalled;
}

exports.clientActivated = function () {
  return workspace.clientActivated;
}

exports.clientFullScreenSet = function () {
  return workspace.clientFullScreenSet;
}

exports.clientSetKeepAbove = function () {
  return workspace.clientSetKeepAbove;
}

exports.numberDesktopsChanged = function () {
  return workspace.numberDesktopsChanged;
}

exports.desktopLayoutChanged = function () {
  return workspace.desktopLayoutChanged;
}

exports.clientDemandsAttentionChanged = function () {
  return workspace.clientDemandsAttentionChanged;
}

exports.numberScreensChanged = function () {
  return workspace.numberScreensChanged;
}

exports.screenResized = function () {
  return workspace.screenResized;
}

exports.currentActivityChanged = function () {
  return workspace.currentActivityChanged;
}

exports.activitiesChanged = function () {
  return workspace.activitiesChanged;
}

exports.activityAdded = function () {
  return workspace.activityAdded;
}

exports.activityRemoved = function () {
  return workspace.activityRemoved;
}

exports.slotSwitchDesktopNext = function () {
  workspace.slotSwitchDesktopNext();
}

exports.slotSwitchDesktopPrevious = function () {
  workspace.slotSwitchDesktopPrevious();
}

exports.slotSwitchDesktopLeft = function () {
  workspace.slotSwitchDesktopLeft();
}
exports.slotSwitchDesktopRight = function () {
  workspace.slotSwitchDesktopRight();
}

exports.slotSwitchDesktopUp = function () {
  workspace.slotSwitchDesktopUp();
}

exports.slotSwitchDesktopDown = function () {
  workspace.slotSwitchDesktopDown();
}

exports.slotSwitchToNextScreen = function () {
  workspace.slotSwitchToNextScreen();
}

exports.slotWindowToNextScreen = function () {
  workspace.slotWindowToNextScreen();
}

exports.slotToggleShowDesktop = function () {
  workspace.slotToggleShowDesktop();
}
 
exports.slotWindowMaximize = function () {
  workspace.slotWindowMaximize();
}
 
exports.slotWindowMaximizeVertical = function () {
  workspace.slotWindowMaximizeVertical();
}
 
exports.slotWindowMaximizeHorizontal = function () {
  workspace.slotWindowMaximizeHorizontal();
}
 
exports.slotWindowMinimize = function () {
  workspace.slotWindowMinimize();
}
 
exports.slotWindowShade = function () {
  workspace.slotWindowShade();
}

exports.slotWindowRaise = function () {
  workspace.slotWindowRaise();
}

exports.slotWindowLower = function () {
  workspace.slotWindowLower();
}

exports.slotWindowRaiseOrLower = function () {
  workspace.slotWindowRaiseOrLower();
}

exports.slotActivateAttentionWindow = function () {
  workspace.slotActivateAttentionWindow();
}

exports.slotWindowPackLeft = function () {
  workspace.slotWindowPackLeft();
}

exports.slotWindowPackRight = function () {
  workspace.slotWindowPackRight();
}

exports.slotWindowPackUp = function () {
  workspace.slotWindowPackUp();
}

exports.slotWindowPackDown = function () {
  workspace.slotWindowPackDown();
}

exports.slotWindowGrowHorizontal = function () {
  workspace.slotWindowGrowHorizontal();
}

exports.slotWindowGrowVertical = function () {
  workspace.slotWindowGrowVertical();
}

exports.slotWindowShrinkHorizontal = function () {
  workspace.slotWindowShrinkHorizontal();
}

exports.slotWindowShrinkVertical = function () {
  workspace.slotWindowShrinkVertical();
}

exports.slotWindowQuickTileLeft = function () {
  workspace.slotWindowQuickTileLeft();
}

exports.slotWindowQuickTileRight = function () {
  workspace.slotWindowQuickTileRight();
}

exports.slotWindowQuickTileTopLeft = function () {
  workspace.slotWindowQuickTileTopLeft();
}

exports.slotWindowQuickTileTopRight = function () {
  workspace.slotWindowQuickTileTopRight();
}

exports.slotWindowQuickTileBottomLeft = function () {
  workspace.slotWindowQuickTileBottomLeft();
}

exports.slotWindowQuickTileBottomRight = function () {
  workspace.slotWindowQuickTileBottomRight();
}

exports.slotSwitchWindowUp = function () {
  workspace.slotSwitchWindowUp();
}

exports.slotSwitchWindowDown = function () {
  workspace.slotSwitchWindowDown();
}

exports.slotSwitchWindowRight = function () {
  workspace.slotSwitchWindowRight();
}

exports.slotSwitchWindowLeft = function () {
  workspace.slotSwitchWindowLeft();
}

exports.slotIncreaseWindowOpacity = function () {
  workspace.slotIncreaseWindowOpacity();
}

exports.slotLowerWindowOpacity = function () {
  workspace.slotLowerWindowOpacity();
}

exports.slotWindowOperations = function () {
  workspace.slotWindowOperations();
}

exports.slotWindowClose = function () {
  workspace.slotWindowClose();
}

exports.slotWindowMove = function () {
  workspace.slotWindowMove();
}

exports.slotWindowResize = function () {
  workspace.slotWindowResize();
}

exports.slotWindowAbove = function () {
  workspace.slotWindowAbove();
}

exports.slotWindowBelow = function () {
  workspace.slotWindowBelow();
}

exports.slotWindowOnAllDesktops = function () {
  workspace.slotWindowOnAllDesktops();
}

exports.slotWindowFullScreen = function () {
  workspace.slotWindowFullScreen();
}

exports.slotWindowNoBorder = function () {
  workspace.slotWindowNoBorder();
}

exports.slotWindowToNextDesktop = function () {
  workspace.slotWindowToNextDesktop();
}

exports.slotWindowToPreviousDesktop = function () {
  workspace.slotWindowToPreviousDesktop();
}

exports.slotWindowToDesktopRight = function () {
  workspace.slotWindowToDesktopRight();
}

exports.slotWindowToDesktopLeft = function () {
  workspace.slotWindowToDesktopLeft();
}

exports.slotWindowToDesktopUp = function () {
  workspace.slotWindowToDesktopUp();
}

exports.slotWindowToDesktopDown = function () {
  workspace.slotWindowToDesktopDown();
}

exports.showOutline = function (rect) {
  return function () {
    workspace.showOutline(rect);
  }
}

exports.hideOutline = function () {
  workspace.hideOutline();
}

exports.clientList = function () {
  return workspace.clientList();
}

exports.clientAreaImpl = function (option) {
  return function (client) {
    return function () {
      return workspace.clientArea(option, screen, desktop);
    }
  }
}

exports.desktopName = function (desktop) {
  return function () {
    return workspace.desktopName();
  }
}

exports.supportInformation = function () {
  return workspace.supportInformation();
}

exports.getClientImpl = function (windowId) {
  return function (Just) {
    return function (Nothing) {
      return function () {
        var client = workspace.getClient(windowId);
        return client ? Just(client) : Nothing;
      }
    }
  }
}
