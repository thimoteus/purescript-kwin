exports.options = function () {
  return options;
}

exports.print = function (str) {
  return function () {
    return print(str);
  }
}

exports.readConfigImpl = function (Just) {
  return function (Nothing) {
    return function (key) {
      return function () {
        var value = readConfig(key, null);
        if (!value) {
          return Nothing;
        } else {
          return Just(value);
        }
      }
    }
  }
}

exports.registerScreenEdge = function (border) {
  return function (callback) {
    return function () {
      return registerScreenEdge(border, callback);
    }
  }
}

exports.registerShortcut = function (title) {
  return function (text) {
    return function (keySequence) {
      return function (callback) {
        return function () {
          return registerShortcut(title, text, keySequence, callback);
        }
      }
    }
  }
}

exports.assert = function (value) {
  return function (message) {
    return function () {
      return assert(value, message);
    }
  }
}


exports.assertFalse = function (value) {
  return function (message) {
    return function () {
      return assertFalse(value, message);
    }
  }
}

exports.callDBusImpl = function (service, path, intface, method, callback) {
  return callDBus(service, path, intface, method, callback);
}