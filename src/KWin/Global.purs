module KWin.Global where

import Prelude

import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Uncurried (EffectFn5, runEffectFn5)

---------
--TYPES--
---------

foreign import data Options :: Type
foreign import data Workspace :: Type
foreign import data KWin :: Type
foreign import data QVariant :: Type
foreign import data ElectricScreen :: Type

newtype Service = Service String
newtype Path = Path String
newtype Interface = Interface String
newtype Method = Method String

----------------------
--EXPORTED FUNCTIONS--
----------------------

foreign import options :: Effect Options

foreign import print :: String -> Effect Unit

readConfig :: String -> Effect (Maybe QVariant)
readConfig = readConfigImpl Just Nothing

foreign import registerScreenEdge :: ElectricScreen -> Effect Unit -> Effect Boolean

foreign import registerShortcut :: String -> String -> String -> Effect Unit -> Effect Boolean

foreign import assert :: Boolean -> String -> Effect Boolean

foreign import assertFalse :: Boolean -> String -> Effect Boolean

assertEqualsBy :: ∀ a. (a -> a -> Boolean) -> a -> a -> String -> Effect Boolean
assertEqualsBy eq fst snd msg =
  assert (fst `eq` snd) msg

assertEquals :: ∀ a. Eq a => a -> a -> String -> Effect Boolean
assertEquals = assertEqualsBy eq

callDBus :: Service -> Path -> Interface -> Method -> Effect Unit -> Effect Unit
callDBus = runEffectFn5 callDBusImpl

-----------------------
--FFI IMPLEMENTATIONS--
-----------------------

foreign import readConfigImpl :: (∀ a. a -> Maybe a) -> (∀ a. Maybe a) -> String -> Effect (Maybe QVariant)

foreign import callDBusImpl :: EffectFn5 Service Path Interface Method (Effect Unit) Unit