module KWin.Options where

import Prelude

import Data.Enum (class BoundedEnum, class Enum, defaultCardinality, defaultPred, defaultSucc)
import Data.Maybe (Maybe(..), fromJust)
import Effect (Effect)
import KWin.Slot (Signal0, Slot, Signal1)
import Partial.Unsafe (unsafePartial)

---------
--ENUMS--
---------

data FocusPolicy
  = ClickToFocus
  | FocusFollowsMouse
  | FocusUnderMouse
  | FocusStrictlyUnderMouse

derive instance eqFocusPolicy :: Eq FocusPolicy
derive instance ordFocusPolicy :: Ord FocusPolicy

instance enumFocusPolicy :: Enum FocusPolicy where
  succ = defaultSucc toFocusPolicy fromFocusPolicy
  pred = defaultPred toFocusPolicy fromFocusPolicy

instance boundedFocusPolicy :: Bounded FocusPolicy where
  top = ClickToFocus
  bottom = FocusStrictlyUnderMouse

instance boundedEnumFocusPolicy :: BoundedEnum FocusPolicy where
  cardinality = defaultCardinality
  toEnum = toFocusPolicy
  fromEnum = fromFocusPolicy

toFocusPolicy :: Int -> Maybe FocusPolicy
toFocusPolicy = go where
  go 0 = Just ClickToFocus
  go 1 = Just FocusFollowsMouse
  go 2 = Just FocusUnderMouse
  go 3 = Just FocusStrictlyUnderMouse
  go _ = Nothing

fromFocusPolicy :: FocusPolicy -> Int
fromFocusPolicy = case _ of
  ClickToFocus -> 0
  FocusFollowsMouse -> 1
  FocusUnderMouse -> 2
  FocusStrictlyUnderMouse -> 3

data MouseCommand
  = MouseRaise
  | MouseLower
  | MouseOperationsMenu
  | MouseToggleRaiseAndLower
  | MouseActivateAndRaise
  | MouseActivateAndLower
  | MouseActivate
  | MouseActivateRaiseAndPassClick
  | MouseActivateAndPassClick
  | MouseMove
  | MouseUnrestrictedMove
  | MouseActivateRaiseAndMove
  | MouseActivateRaiseAndUnrestrictedMove
  | MouseResize
  | MouseUnrestrictedResize
  | MouseShade
  | MouseSetShade
  | MouseUnsetShade
  | MouseMaximize
  | MouseRestore
  | MouseMinimize
  | MouseNextDesktop
  | MousePreviousDesktop
  | MouseAbove
  | MouseBelow
  | MouseOpacityMore
  | MouseOpacityLess
  | MouseClose
  | MousePreviousTab
  | MouseNextTab
  | MouseDragTab
  | MouseNothing

derive instance eqMouseCommand :: Eq MouseCommand
derive instance ordMouseCommand :: Ord MouseCommand

instance enumMouseCommand :: Enum MouseCommand where
  succ = defaultSucc toMouseCommand fromMouseCommand
  pred = defaultPred toMouseCommand fromMouseCommand

instance boundedMouseCommand :: Bounded MouseCommand where
  top = MouseRaise
  bottom = MouseNothing

instance boundedEnumMouseCommand :: BoundedEnum MouseCommand where
  cardinality = defaultCardinality
  toEnum = toMouseCommand
  fromEnum = fromMouseCommand

toMouseCommand :: Int -> Maybe MouseCommand
toMouseCommand = go where
  go 0 = Just MouseRaise
  go 1 = Just MouseLower
  go 2 = Just MouseOperationsMenu
  go 3 = Just MouseToggleRaiseAndLower
  go 4 = Just MouseActivateAndRaise
  go 5 = Just MouseActivateAndLower
  go 6 = Just MouseActivate
  go 7 = Just MouseActivateRaiseAndPassClick
  go 8 = Just MouseActivateAndPassClick
  go 9 = Just MouseMove
  go 10 = Just MouseUnrestrictedMove
  go 11 = Just MouseActivateRaiseAndMove
  go 12 = Just MouseActivateRaiseAndUnrestrictedMove
  go 13 = Just MouseResize
  go 14 = Just MouseUnrestrictedResize
  go 15 = Just MouseShade
  go 16 = Just MouseSetShade
  go 17 = Just MouseUnsetShade
  go 18 = Just MouseMaximize
  go 19 = Just MouseRestore
  go 20 = Just MouseMinimize
  go 21 = Just MouseNextDesktop
  go 22 = Just MousePreviousDesktop
  go 23 = Just MouseAbove
  go 24 = Just MouseBelow
  go 25 = Just MouseOpacityMore
  go 26 = Just MouseOpacityLess
  go 27 = Just MouseClose
  go 28 = Just MousePreviousTab
  go 29 = Just MouseNextTab
  go 30 = Just MouseDragTab
  go 31 = Just MouseNothing
  go _ = Nothing

fromMouseCommand :: MouseCommand -> Int
fromMouseCommand = case _ of
  MouseRaise -> 0 
  MouseLower -> 1 
  MouseOperationsMenu -> 2
  MouseToggleRaiseAndLower -> 3
  MouseActivateAndRaise -> 4
  MouseActivateAndLower -> 5
  MouseActivate -> 6
  MouseActivateRaiseAndPassClick -> 7
  MouseActivateAndPassClick -> 8
  MouseMove -> 9
  MouseUnrestrictedMove -> 10
  MouseActivateRaiseAndMove -> 11
  MouseActivateRaiseAndUnrestrictedMove -> 12
  MouseResize -> 13
  MouseUnrestrictedResize -> 14
  MouseShade -> 15
  MouseSetShade -> 16
  MouseUnsetShade -> 17
  MouseMaximize -> 18
  MouseRestore -> 19
  MouseMinimize -> 20
  MouseNextDesktop -> 21
  MousePreviousDesktop -> 22
  MouseAbove -> 23
  MouseBelow -> 24
  MouseOpacityMore -> 25
  MouseOpacityLess -> 26
  MouseClose -> 27
  MousePreviousTab -> 28
  MouseNextTab -> 29
  MouseDragTab -> 30
  MouseNothing -> 31

data MouseWheelCommand
  = MouseWheelRaiseLower
  |	MouseWheelShadeUnshade
  |	MouseWheelMaximizeRestore
  |	MouseWheelAboveBelow
  | MouseWheelPreviousNextDesktop
  |	MouseWheelChangeOpacity
  |	MouseWheelChangeCurrentTab
  |	MouseWheelNothing

derive instance eqMouseWheelCommand :: Eq MouseWheelCommand
derive instance ordMouseWheelCommand :: Ord MouseWheelCommand

instance enumMouseWheelCommand :: Enum MouseWheelCommand where
  succ = defaultSucc toMouseWheelCommand fromMouseWheelCommand
  pred = defaultPred toMouseWheelCommand fromMouseWheelCommand

instance boundedMouseWheelCommand :: Bounded MouseWheelCommand where
  top = MouseWheelRaiseLower
  bottom = MouseWheelNothing

instance boundedEnumMouseWheelCommand :: BoundedEnum MouseWheelCommand where
  cardinality = defaultCardinality
  toEnum = toMouseWheelCommand
  fromEnum = fromMouseWheelCommand

toMouseWheelCommand :: Int -> Maybe MouseWheelCommand
toMouseWheelCommand = go
  where
  go 0 = Just MouseWheelRaiseLower
  go 1 = Just	MouseWheelShadeUnshade
  go 2 = Just	MouseWheelMaximizeRestore
  go 3 = Just	MouseWheelAboveBelow
  go 4 = Just MouseWheelPreviousNextDesktop
  go 5 = Just	MouseWheelChangeOpacity
  go 6 = Just	MouseWheelChangeCurrentTab
  go 7 = Just	MouseWheelNothing
  go _ = Nothing

fromMouseWheelCommand :: MouseWheelCommand -> Int
fromMouseWheelCommand = case _ of
  MouseWheelRaiseLower -> 0 
  MouseWheelShadeUnshade -> 1 
  MouseWheelMaximizeRestore -> 2 
  MouseWheelAboveBelow -> 3 
  MouseWheelPreviousNextDesktop -> 4 
  MouseWheelChangeOpacity -> 5 
  MouseWheelChangeCurrentTab -> 6 
  MouseWheelNothing -> 7 

data GISwapStrategy
  = NoSwapEncourage
  |	CopyFrontBuffer
  |	PaintFullScreen
  |	ExtendDamage
  |	AutoSwapStrategy

derive instance eqGISwapStrategy :: Eq GISwapStrategy
derive instance ordGISwapStrategy :: Ord GISwapStrategy

instance enumGISwapStrategy :: Enum GISwapStrategy where
  succ = defaultSucc toGISwapStrategy fromGISwapStrategy
  pred = defaultPred toGISwapStrategy fromGISwapStrategy

instance boundedGISwapStrategy :: Bounded GISwapStrategy where
  top = NoSwapEncourage
  bottom = AutoSwapStrategy

instance boundedEnumGISwapStrategy :: BoundedEnum GISwapStrategy where
  cardinality = defaultCardinality
  toEnum = toGISwapStrategy
  fromEnum = fromGISwapStrategy

toGISwapStrategy :: Int -> Maybe GISwapStrategy
toGISwapStrategy = go
  where
  go 0 = Just NoSwapEncourage
  go 1 = Just CopyFrontBuffer
  go 2 = Just PaintFullScreen
  go 3 = Just ExtendDamage
  go 4 = Just AutoSwapStrategy
  go _ = Nothing

fromGISwapStrategy :: GISwapStrategy -> Int
fromGISwapStrategy = case _ of
  NoSwapEncourage -> 0
  CopyFrontBuffer -> 1
  PaintFullScreen -> 2
  ExtendDamage -> 3
  AutoSwapStrategy -> 4

--------------
--PROPERTIES--
--------------

getFocusPropertyIsReasonable :: Effect Boolean
getFocusPropertyIsReasonable = unsafeGetProperty "focusPropertyIsReasonable"

getFocusPolicy :: Effect FocusPolicy
getFocusPolicy = unsafePartial (fromJust <<< toFocusPolicy) <$> unsafeGetProperty "focusPolicy"

setFocusPolicy :: FocusPolicy -> Effect Unit
setFocusPolicy = unsafeSetProperty "focusPolicy" <<< fromFocusPolicy

getNextFocusPrefersMouse :: Effect Boolean
getNextFocusPrefersMouse = unsafeGetProperty "nextFocusPrefersMouse"

setNextFocusPrefersMouse :: Boolean -> Effect Unit
setNextFocusPrefersMouse = unsafeSetProperty "nextFocusPrefersMouse"

getClickRaise :: Effect Boolean
getClickRaise = unsafeGetProperty "clickRaise"

setClickRaise :: Boolean -> Effect Unit
setClickRaise = unsafeSetProperty "clickRaise"

getAutoRaise :: Effect Boolean
getAutoRaise = unsafeGetProperty "autoRaise"

setAutoRaise :: Boolean -> Effect Unit
setAutoRaise = unsafeSetProperty "autoRaise"

getAutoRaiseInterval :: Effect Int
getAutoRaiseInterval = unsafeGetProperty "autoRaiseInterval"

setAutoRaiseInterval :: Int -> Effect Unit
setAutoRaiseInterval = unsafeSetProperty "autoRaiseInterval"

getDelayFocusInterval :: Effect Int
getDelayFocusInterval = unsafeGetProperty "delayFocusInterval"

setDelayFocusInterval :: Int -> Effect Unit
setDelayFocusInterval = unsafeSetProperty "delayFocusInterval"

getShadeHover :: Effect Boolean
getShadeHover = unsafeGetProperty "shadeHover"

setShadeHover :: Boolean -> Effect Unit
setShadeHover = unsafeSetProperty "shadeHover"

getShadeHoverInterval :: Effect Int
getShadeHoverInterval = unsafeGetProperty "shadeHoverInterval"

setShadeHoverInterval :: Int -> Effect Unit
setShadeHoverInterval = unsafeSetProperty "shadeHoverInterval"

getSeparateScreenFocus :: Effect Int
getSeparateScreenFocus = unsafeGetProperty "separateScreenFocus"

setSeparateScreenFocus :: Int -> Effect Unit
setSeparateScreenFocus = unsafeSetProperty "separateScreenFocus"

getPlacement :: Effect Int
getPlacement = unsafeGetProperty "placement"

setPlacement :: Int -> Effect Unit
setPlacement = unsafeSetProperty "placement"

getBorderSnapZone :: Effect Int
getBorderSnapZone = unsafeGetProperty "borderSnapZone"

setBorderSnapZone :: Int -> Effect Unit
setBorderSnapZone = unsafeSetProperty "borderSnapZone"

getWindowSnapZone :: Effect Int
getWindowSnapZone = unsafeGetProperty "windowSnapZone"

setWindowSnapZone :: Int -> Effect Unit
setWindowSnapZone = unsafeSetProperty "windowSnapZone"

getCenterSnapZone :: Effect Int
getCenterSnapZone = unsafeGetProperty "centerSnapZone"

setCenterSnapZone :: Int -> Effect Unit
setCenterSnapZone = unsafeSetProperty "centerSnapZone"

getSnapOnlyWhenOverlapping :: Effect Boolean
getSnapOnlyWhenOverlapping = unsafeGetProperty "snapOnlyWhenOverlapping"

setSnapOnlyWhenOverlapping :: Boolean -> Effect Unit
setSnapOnlyWhenOverlapping = unsafeSetProperty "snapOnlyWhenOverlapping"

getShowDesktopIsMinimizeAll :: Effect Boolean
getShowDesktopIsMinimizeAll = unsafeGetProperty "showDesktopIsMinimizeAll"

setShowDesktopIsMinimizeAll :: Boolean -> Effect Unit
setShowDesktopIsMinimizeAll = unsafeSetProperty "showDesktopIsMinimizeAll"

getRollOverDesktops :: Effect Boolean
getRollOverDesktops = unsafeGetProperty "rollOverDesktops"

setRollOverDesktops :: Boolean -> Effect Unit
setRollOverDesktops = unsafeSetProperty "rollOverDesktops"

getFocusStealingPreventionLevel :: Effect Int
getFocusStealingPreventionLevel = unsafeGetProperty "focusStealingPreventionLevel"

setFocusStealingPreventionLevel :: Int -> Effect Unit
setFocusStealingPreventionLevel = unsafeSetProperty "focusStealingPreventionLevel"

getLegacyFullscreenSupport :: Effect Boolean
getLegacyFullscreenSupport = unsafeGetProperty "legacyFullscreenSupport"

setLegacyFullscreenSupport :: Boolean -> Effect Unit
setLegacyFullscreenSupport = unsafeSetProperty "legacyFullscreenSupport"

-- TODO: operationTitlebarDblClick

getCommandActiveTitleBar1 :: Effect MouseCommand
getCommandActiveTitleBar1 = unsafePartial (fromJust <<< toMouseCommand) <$> unsafeGetProperty "commandActiveTitleBar1"

setCommandActiveTitleBar1 :: MouseCommand -> Effect Unit
setCommandActiveTitleBar1 = unsafeSetProperty "commandActiveTitleBar1" <<< fromMouseCommand

getCommandActiveTitleBar2 :: Effect MouseCommand
getCommandActiveTitleBar2 = unsafePartial (fromJust <<< toMouseCommand) <$> unsafeGetProperty "commandActiveTitleBar2"

setCommandActiveTitleBar2 :: MouseCommand -> Effect Unit
setCommandActiveTitleBar2 = unsafeSetProperty "commandActiveTitleBar2" <<< fromMouseCommand

getCommandActiveTitleBar3 :: Effect MouseCommand
getCommandActiveTitleBar3 = unsafePartial (fromJust <<< toMouseCommand) <$> unsafeGetProperty "commandActiveTitleBar3"

setCommandActiveTitleBar3 :: MouseCommand -> Effect Unit
setCommandActiveTitleBar3 = unsafeSetProperty "commandActiveTitleBar3" <<< fromMouseCommand

getCommandInactiveTitleBar1 :: Effect MouseCommand
getCommandInactiveTitleBar1 = unsafePartial (fromJust <<< toMouseCommand) <$> unsafeGetProperty "commandInactiveTitleBar1"

setCommandInactiveTitleBar1 :: MouseCommand -> Effect Unit
setCommandInactiveTitleBar1 = unsafeSetProperty "commandInactiveTitleBar1" <<< fromMouseCommand

getCommandInactiveTitleBar2 :: Effect MouseCommand
getCommandInactiveTitleBar2 = unsafePartial (fromJust <<< toMouseCommand) <$> unsafeGetProperty "commandInactiveTitleBar2"

setCommandInactiveTitleBar2 :: MouseCommand -> Effect Unit
setCommandInactiveTitleBar2 = unsafeSetProperty "commandInactiveTitleBar2" <<< fromMouseCommand

getCommandInactiveTitleBar3 :: Effect MouseCommand
getCommandInactiveTitleBar3 = unsafePartial (fromJust <<< toMouseCommand) <$> unsafeGetProperty "commandInactiveTitleBar3"

setCommandInactiveTitleBar3 :: MouseCommand -> Effect Unit
setCommandInactiveTitleBar3 = unsafeSetProperty "commandInactiveTitleBar3" <<< fromMouseCommand

getCommandWindow1 :: Effect MouseCommand
getCommandWindow1 = unsafePartial (fromJust <<< toMouseCommand) <$> unsafeGetProperty "commandWindow1"

setCommandWindow1 :: MouseCommand -> Effect Unit
setCommandWindow1 = unsafeSetProperty "commandWindow1" <<< fromMouseCommand

getCommandWindow2 :: Effect MouseCommand
getCommandWindow2 = unsafePartial (fromJust <<< toMouseCommand) <$> unsafeGetProperty "commandWindow2"

setCommandWindow2 :: MouseCommand -> Effect Unit
setCommandWindow2 = unsafeSetProperty "commandWindow2" <<< fromMouseCommand

getCommandWindow3 :: Effect MouseCommand
getCommandWindow3 = unsafePartial (fromJust <<< toMouseCommand) <$> unsafeGetProperty "commandWindow3"

setCommandWindow3 :: MouseCommand -> Effect Unit
setCommandWindow3 = unsafeSetProperty "commandWindow3" <<< fromMouseCommand

getCommandWindowWheel :: Effect MouseCommand
getCommandWindowWheel = unsafePartial (fromJust <<< toMouseCommand) <$> unsafeGetProperty "commandWindowWheel"

setCommandWindowWheel :: MouseCommand -> Effect Unit
setCommandWindowWheel = unsafeSetProperty "commandWindowWheel" <<< fromMouseCommand

getCommandAll1 :: Effect MouseCommand
getCommandAll1 = unsafePartial (fromJust <<< toMouseCommand) <$> unsafeGetProperty "commandAll1"

setCommandAll1 :: MouseCommand -> Effect Unit
setCommandAll1 = unsafeSetProperty "commandAll1" <<< fromMouseCommand

getCommandAll2 :: Effect MouseCommand
getCommandAll2 = unsafePartial (fromJust <<< toMouseCommand) <$> unsafeGetProperty "commandAll2"

setCommandAll2 :: MouseCommand -> Effect Unit
setCommandAll2 = unsafeSetProperty "commandAll2" <<< fromMouseCommand

getCommandAll3 :: Effect MouseCommand
getCommandAll3 = unsafePartial (fromJust <<< toMouseCommand) <$> unsafeGetProperty "commandAll3"

setCommandAll3 :: MouseCommand -> Effect Unit
setCommandAll3 = unsafeSetProperty "commandAll3" <<< fromMouseCommand

getKeyCmdAllModKey :: Effect Int
getKeyCmdAllModKey = unsafeGetProperty "keyCmdAllModKey"

setKeyCmdAllModKey :: Int -> Effect Unit
setKeyCmdAllModKey = unsafeSetProperty "keyCmdAllModKey"

getShowGeometryTip :: Effect Boolean
getShowGeometryTip = unsafeGetProperty "showGeometryTip"

setShowGeometryTip :: Boolean -> Effect Unit
setShowGeometryTip = unsafeSetProperty "showGeometryTip"

getCondensedTitle :: Effect Boolean
getCondensedTitle = unsafeGetProperty "condensedTitle"

setCondensedTitle :: Boolean -> Effect Unit
setCondensedTitle = unsafeSetProperty "condensedTitle"

getElectricBorderMaximize :: Effect Boolean
getElectricBorderMaximize = unsafeGetProperty "electricBorderMaximize"

setElectricBorderMaximize :: Boolean -> Effect Unit
setElectricBorderMaximize = unsafeSetProperty "electricBorderMaximize"

getElectricBorderTiling :: Effect Boolean
getElectricBorderTiling = unsafeGetProperty "electricBorderTiling"

setElectricBorderTiling :: Boolean -> Effect Unit
setElectricBorderTiling = unsafeSetProperty "electricBorderTiling"

getElectricBorderCornerRatio :: Effect Number
getElectricBorderCornerRatio = unsafeGetProperty "electricBorderCornerRatio"

setElectricBorderCornerRatio :: Number -> Effect Unit
setElectricBorderCornerRatio = unsafeSetProperty "electricBorderCornerRatio"

getBorderlessMaximizedWindows :: Effect Boolean
getBorderlessMaximizedWindows = unsafeGetProperty "borderlessMaximizedWindows"

setBorderlessMaximizedWindows :: Boolean -> Effect Unit
setBorderlessMaximizedWindows = unsafeSetProperty "borderlessMaximizedWindows"

getKillPingTimeout :: Effect Int
getKillPingTimeout = unsafeGetProperty "killPingTimeout"

setKillPingTimeout :: Int -> Effect Unit
setKillPingTimeout = unsafeSetProperty "killPingTimeout"

getHideUtilityWindowsForInactive :: Effect Boolean
getHideUtilityWindowsForInactive = unsafeGetProperty "hideUtilityWindowsForInactive"

setHideUtilityWindowsForInactive :: Boolean -> Effect Unit
setHideUtilityWindowsForInactive = unsafeSetProperty "hideUtilityWindowsForInactive"

getInactiveTabsSkipTaskbar :: Effect Boolean
getInactiveTabsSkipTaskbar = unsafeGetProperty "inactiveTabsSkipTaskbar"

setInactiveTabsSkipTaskbar :: Boolean -> Effect Unit
setInactiveTabsSkipTaskbar = unsafeSetProperty "inactiveTabsSkipTaskbar"

getAutogroupSimilarWindows :: Effect Boolean
getAutogroupSimilarWindows = unsafeGetProperty "autogroupSimilarWindows"

setAutogroupSimilarWindows :: Boolean -> Effect Unit
setAutogroupSimilarWindows = unsafeSetProperty "autogroupSimilarWindows"

getAutogroupInForeground :: Effect Boolean
getAutogroupInForeground = unsafeGetProperty "autogroupInForeground"

setAutogroupInForeground :: Boolean -> Effect Unit
setAutogroupInForeground = unsafeSetProperty "autogroupInForeground"

getCompositingMode :: Effect Int
getCompositingMode = unsafeGetProperty "compositingMode"

setCompositingMode :: Int -> Effect Unit
setCompositingMode = unsafeSetProperty "compositingMode"

getUseCompositing :: Effect Boolean
getUseCompositing = unsafeGetProperty "useCompositing"

setUseCompositing :: Boolean -> Effect Unit
setUseCompositing = unsafeSetProperty "useCompositing"

getCompositingInitialized :: Effect Boolean
getCompositingInitialized = unsafeGetProperty "compositingInitialized"

setCompositingInitialized :: Boolean -> Effect Unit
setCompositingInitialized = unsafeSetProperty "compositingInitialized"

getHiddenPreviews :: Effect Int
getHiddenPreviews = unsafeGetProperty "hiddenPreviews"

setHiddenPreviews :: Int -> Effect Unit
setHiddenPreviews = unsafeSetProperty "hiddenPreviews"

getUnredirectFullscreen :: Effect Boolean
getUnredirectFullscreen = unsafeGetProperty "unredirectFullscreen"

setUnredirectFullscreen :: Boolean -> Effect Unit
setUnredirectFullscreen = unsafeSetProperty "unredirectFullscreen"

getGlSmoothScale :: Effect Int
getGlSmoothScale = unsafeGetProperty "glSmoothScale"

setGlSmoothScale :: Int -> Effect Unit
setGlSmoothScale = unsafeSetProperty "glSmoothScale"

getColorCorrected :: Effect Boolean
getColorCorrected = unsafeGetProperty "colorCorrected"

setColorCorrected :: Boolean -> Effect Unit
setColorCorrected = unsafeSetProperty "colorCorrected"

getXrenderSmoothScale :: Effect Boolean
getXrenderSmoothScale = unsafeGetProperty "xrenderSmoothScale"

setXrenderSmoothScale :: Boolean -> Effect Unit
setXrenderSmoothScale = unsafeSetProperty "xrenderSmoothScale"

getMaxFpsInterval :: Effect Int
getMaxFpsInterval = unsafeGetProperty "maxFpsInterval"

setMaxFpsInterval :: Int -> Effect Unit
setMaxFpsInterval = unsafeSetProperty "maxFpsInterval"

getRefreshRate :: Effect Int
getRefreshRate = unsafeGetProperty "refreshRate"

setRefreshRate :: Int -> Effect Unit
setRefreshRate = unsafeSetProperty "refreshRate"

getVBlankTime :: Effect Int
getVBlankTime = unsafeGetProperty "vBlankTime"

setVBlankTime :: Int -> Effect Unit
setVBlankTime = unsafeSetProperty "vBlankTime"

getGlDirect :: Effect Boolean
getGlDirect = unsafeGetProperty "glDirect"

setGlDirect :: Boolean -> Effect Unit
setGlDirect = unsafeSetProperty "glDirect"

getGlStrictBinding :: Effect Boolean
getGlStrictBinding = unsafeGetProperty "glStrictBinding"

setGlStrictBinding :: Boolean -> Effect Unit
setGlStrictBinding = unsafeSetProperty "glStrictBinding"

getGlStrictBindingFollowsDriver :: Effect Boolean
getGlStrictBindingFollowsDriver = unsafeGetProperty "glStrictBindingFollowsDriver"

setGlStrictBindingFollowsDriver :: Boolean -> Effect Unit
setGlStrictBindingFollowsDriver = unsafeSetProperty "glStrictBindingFollowsDriver"

getGlLegacy :: Effect Boolean
getGlLegacy = unsafeGetProperty "glLegacy"

setGlLegacy :: Boolean -> Effect Unit
setGlLegacy = unsafeSetProperty "glLegacy"

getGlCoreProfile :: Effect Boolean
getGlCoreProfile = unsafeGetProperty "glCoreProfile"

setGlCoreProfile :: Boolean -> Effect Unit
setGlCoreProfile = unsafeSetProperty "glCoreProfile"

getGlPreferBufferSwap :: Effect GISwapStrategy
getGlPreferBufferSwap = unsafePartial (fromJust <<< toGISwapStrategy) <$> unsafeGetProperty "glPreferBufferSwap"

setGlPreferBufferSwap :: GISwapStrategy -> Effect Unit
setGlPreferBufferSwap = unsafeSetProperty "glPreferBufferSwap" <<< fromGISwapStrategy

---------
--SLOTS--
---------

configChanged :: Effect (Slot Signal0)
configChanged = unsafeGetSlot "configChanged"

focusPolicyChanged :: Effect (Slot Signal0)
focusPolicyChanged = unsafeGetSlot "focusPolicyChanged"

nextFocusPrefersMouseChanged :: Effect (Slot Signal0)
nextFocusPrefersMouseChanged = unsafeGetSlot "nextFocusPrefersMouseChanged"

clickRaiseChanged :: Effect (Slot Signal0)
clickRaiseChanged = unsafeGetSlot "clickRaiseChanged"

autoRaiseChanged :: Effect (Slot Signal0)
autoRaiseChanged = unsafeGetSlot "autoRaiseChanged"

autoRaiseIntervalChanged :: Effect (Slot Signal0)
autoRaiseIntervalChanged = unsafeGetSlot "autoRaiseIntervalChanged"

delayFocusIntervalChanged :: Effect (Slot Signal0)
delayFocusIntervalChanged = unsafeGetSlot "delayFocusIntervalChanged"

shadeHoverChanged :: Effect (Slot Signal0)
shadeHoverChanged = unsafeGetSlot "shadeHoverChanged"

shadeHoverIntervalChanged :: Effect (Slot Signal0)
shadeHoverIntervalChanged = unsafeGetSlot "shadeHoverIntervalChanged"

separateScreenFocusChanged :: Effect (Slot (Signal1 Boolean))
separateScreenFocusChanged = unsafeGetSlot "separateScreenFocusChanged"

placementChanged :: Effect (Slot Signal0)
placementChanged = unsafeGetSlot "placementChanged"

borderSnapZoneChanged :: Effect (Slot Signal0)
borderSnapZoneChanged = unsafeGetSlot "borderSnapZoneChanged"

windowSnapZoneChanged :: Effect (Slot Signal0)
windowSnapZoneChanged = unsafeGetSlot "windowSnapZoneChanged"

centerSnapZoneChanged :: Effect (Slot Signal0)
centerSnapZoneChanged = unsafeGetSlot "centerSnapZoneChanged"

snapOnlyWhenOverlappingChanged :: Effect (Slot Signal0)
snapOnlyWhenOverlappingChanged = unsafeGetSlot "snapOnlyWhenOverlappingChanged"

showDesktopIsMinimizeAllChanged :: Effect (Slot Signal0)
showDesktopIsMinimizeAllChanged = unsafeGetSlot "showDesktopIsMinimizeAllChanged"

rollOverDesktopsChanged :: Effect (Slot (Signal1 Boolean))
rollOverDesktopsChanged = unsafeGetSlot "rollOverDesktopsChanged"

focusStealingPreventionLevelChanged :: Effect (Slot Signal0)
focusStealingPreventionLevelChanged = unsafeGetSlot "focusStealingPreventionLevelChanged"

legacyFullscreenSupportChanged :: Effect (Slot Signal0)
legacyFullscreenSupportChanged = unsafeGetSlot "legacyFullscreenSupportChanged"

operationTitlebarDblClickChanged :: Effect (Slot Signal0)
operationTitlebarDblClickChanged = unsafeGetSlot "operationTitlebarDblClickChanged"

commandActiveTitlebar1Changed :: Effect (Slot Signal0)
commandActiveTitlebar1Changed = unsafeGetSlot "commandActiveTitlebar1Changed"

commandActiveTitlebar2Changed :: Effect (Slot Signal0)
commandActiveTitlebar2Changed = unsafeGetSlot "commandActiveTitlebar2Changed"

commandActiveTitlebar3Changed :: Effect (Slot Signal0)
commandActiveTitlebar3Changed = unsafeGetSlot "commandActiveTitlebar3Changed"

commandInactiveTitlebar1Changed :: Effect (Slot Signal0)
commandInactiveTitlebar1Changed = unsafeGetSlot "commandInactiveTitlebar1Changed"

commandInactiveTitlebar2Changed :: Effect (Slot Signal0)
commandInactiveTitlebar2Changed = unsafeGetSlot "commandInactiveTitlebar2Changed"

commandInactiveTitlebar3Changed :: Effect (Slot Signal0)
commandInactiveTitlebar3Changed = unsafeGetSlot "commandInactiveTitlebar3Changed"

commandWindow1Changed :: Effect (Slot Signal0)
commandWindow1Changed = unsafeGetSlot "commandWindow1Changed"

commandWindow2Changed :: Effect (Slot Signal0)
commandWindow2Changed = unsafeGetSlot "commandWindow2Changed"

commandWindow3Changed :: Effect (Slot Signal0)
commandWindow3Changed = unsafeGetSlot "commandWindow3Changed"

commandWindowWheelChanged :: Effect (Slot Signal0)
commandWindowWheelChanged = unsafeGetSlot "commandWindowWheelChanged"

commandAll1Changed :: Effect (Slot Signal0)
commandAll1Changed = unsafeGetSlot "commandAll1Changed"

commandAll2Changed :: Effect (Slot Signal0)
commandAll2Changed = unsafeGetSlot "commandAll2Changed"

commandAll3Changed :: Effect (Slot Signal0)
commandAll3Changed = unsafeGetSlot "commandAll3Changed"

keyCmdAllModKeyChanged :: Effect (Slot Signal0)
keyCmdAllModKeyChanged = unsafeGetSlot "keyCmdAllModKeyChanged"

showGeometryTipChanged :: Effect (Slot Signal0)
showGeometryTipChanged = unsafeGetSlot "showGeometryTipChanged"

condensedTitleChanged :: Effect (Slot Signal0)
condensedTitleChanged = unsafeGetSlot "condensedTitleChanged"

electricBorderMaximizeChanged :: Effect (Slot Signal0)
electricBorderMaximizeChanged = unsafeGetSlot "electricBorderMaximizeChanged"

electricBorderTilingChanged :: Effect (Slot Signal0)
electricBorderTilingChanged = unsafeGetSlot "electricBorderTilingChanged"

electricBorderCornerRatioChanged :: Effect (Slot Signal0)
electricBorderCornerRatioChanged = unsafeGetSlot "electricBorderCornerRatioChanged"

borderlessMaximizedWindowsChanged :: Effect (Slot Signal0)
borderlessMaximizedWindowsChanged = unsafeGetSlot "borderlessMaximizedWindowsChanged"

killPingTimeoutChanged :: Effect (Slot Signal0)
killPingTimeoutChanged = unsafeGetSlot "killPingTimeoutChanged"

hideUtilityWindowsForInactiveChanged :: Effect (Slot Signal0)
hideUtilityWindowsForInactiveChanged = unsafeGetSlot "hideUtilityWindowsForInactiveChanged"

inactiveTabsSkipTaskbarChanged :: Effect (Slot Signal0)
inactiveTabsSkipTaskbarChanged = unsafeGetSlot "inactiveTabsSkipTaskbarChanged"

autogroupSimilarWindowsChanged :: Effect (Slot Signal0)
autogroupSimilarWindowsChanged = unsafeGetSlot "autogroupSimilarWindowsChanged"

autogroupInForegroundChanged :: Effect (Slot Signal0)
autogroupInForegroundChanged = unsafeGetSlot "autogroupInForegroundChanged"

compositingModeChanged :: Effect (Slot Signal0)
compositingModeChanged = unsafeGetSlot "compositingModeChanged"

useCompositingChanged :: Effect (Slot Signal0)
useCompositingChanged = unsafeGetSlot "useCompositingChanged"

compositingInitializedChanged :: Effect (Slot Signal0)
compositingInitializedChanged = unsafeGetSlot "compositingInitializedChanged"

hiddenPreviewsChanged :: Effect (Slot Signal0)
hiddenPreviewsChanged = unsafeGetSlot "hiddenPreviewsChanged"

unredirectFullscreenChanged :: Effect (Slot Signal0)
unredirectFullscreenChanged = unsafeGetSlot "unredirectFullscreenChanged"

glSmoothScaleChanged :: Effect (Slot Signal0)
glSmoothScaleChanged = unsafeGetSlot "glSmoothScaleChanged"

colorCorrectedChanged :: Effect (Slot Signal0)
colorCorrectedChanged = unsafeGetSlot "colorCorrectedChanged"

xrenderSmoothScaleChanged :: Effect (Slot Signal0)
xrenderSmoothScaleChanged = unsafeGetSlot "xrenderSmoothScaleChanged"

maxFpsIntervalChanged :: Effect (Slot Signal0)
maxFpsIntervalChanged = unsafeGetSlot "maxFpsIntervalChanged"

refreshRateChanged :: Effect (Slot Signal0)
refreshRateChanged = unsafeGetSlot "refreshRateChanged"

vBlankTimeChanged :: Effect (Slot Signal0)
vBlankTimeChanged = unsafeGetSlot "vBlankTimeChanged"

glDirectChanged :: Effect (Slot Signal0)
glDirectChanged = unsafeGetSlot "glDirectChanged"

glStrictBindingChanged :: Effect (Slot Signal0)
glStrictBindingChanged = unsafeGetSlot "glStrictBindingChanged"

glStrictBindingFollowsDriverChanged :: Effect (Slot Signal0)
glStrictBindingFollowsDriverChanged = unsafeGetSlot "glStrictBindingFollowsDriverChanged"

glLegacyChanged :: Effect (Slot Signal0)
glLegacyChanged = unsafeGetSlot "glLegacyChanged"

glCoreProfileChanged :: Effect (Slot Signal0)
glCoreProfileChanged = unsafeGetSlot "glCoreProfileChanged"

glPreferBufferSwapChanged :: Effect (Slot Signal0)
glPreferBufferSwapChanged = unsafeGetSlot "glPreferBufferSwapChanged"

-----------------------
--FFI IMPLEMENTATIONS--
-----------------------

foreign import unsafeGetProperty :: ∀ a. String -> Effect a
foreign import unsafeSetProperty :: ∀ a. String -> a -> Effect Unit
foreign import unsafeGetSlot :: ∀ a. String -> Effect (Slot a)