module Effect.Console where

import Prelude

import Effect (Effect)

foreign import log :: String -> Effect Unit

logShow :: ∀ a. Show a => a -> Effect Unit
logShow = log <<< show