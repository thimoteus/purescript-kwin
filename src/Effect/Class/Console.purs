module Effect.Class.Console where

import Prelude

import Effect.Class (class MonadEffect, liftEffect)
import Effect.Console as Effect.Console

log :: ∀ m. MonadEffect m => String -> m Unit
log = liftEffect <<< Effect.Console.log

logShow :: ∀ a m. MonadEffect m => Show a => a -> m Unit
logShow = liftEffect <<< Effect.Console.logShow